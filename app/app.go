package app

import (
	"fmt"
	"log"

	"github.com/rivo/tview"
	"gitlab.com/muffehazard/chatclient/app/loglevel"
	"gitlab.com/muffehazard/chatclient/app/pages"
	"gitlab.com/muffehazard/chatclient/app/pages/channel"
)

type appErr string

func (e appErr) Error() string { return string(e) }

const NoChannels = appErr("No channels")
const ChannelExists = appErr("Channel already exists")
const ChannelNotExists = appErr("Channel doesn't exist")

const (
	INFOPAGE    = "info"
	CONNECTFORM = "connect form"
	KEYFORM     = "key form"
)

// Visual root of program
type App struct {
	app         *tview.Application
	pages       *tview.Pages
	infoPage    *pages.InfoPage
	connectForm *pages.ConnectForm
	keyForm     *pages.KeyForm
	channels    map[string]*pages.ChannelPage
}

func New() *App {
	app := &App{
		app:      tview.NewApplication(),
		pages:    tview.NewPages(),
		channels: make(map[string]*pages.ChannelPage),
	}

	app.infoPage = pages.NewInfoPage(app)
	app.connectForm = pages.NewConnectForm(app)
	app.keyForm = pages.NewKeyForm(app)

	app.pages.AddPage(INFOPAGE, app.infoPage, true, false)
	app.pages.AddPage(CONNECTFORM, app.connectForm, true, true)
	app.pages.AddPage(KEYFORM, app.keyForm, true, false)

	app.app.SetRoot(app.pages, true)

	return app
}

// Private methods
func (app *App) changePage(page string) {
	app.pages.SwitchToPage(page)
	app.app.Draw()
}

// Public methods
func (app *App) Run() error {
	return app.app.Run()
}

func (app *App) Stop() {
	app.app.Stop()
}

// Interface methods
func (app *App) WriteMsg(format string, v ...interface{}) {
	err := app.infoPage.WriteMsg(format, v...)
	if err != nil {
		app.app.Stop()
		log.Printf("Failed app output write: %v", err)
	}
}

func (app *App) WriteErrMsg(format string, v ...interface{}) {
	app.WriteMsg("[ERROR] "+format, v...)
	app.changePage(INFOPAGE)
}

func (app *App) ChangeChannel(label string) error {
	_, ok := app.channels[label]
	if !ok {
		return fmt.Errorf("No such channel: %v", label)
	}

	app.changePage(label)
	return nil
}

func (app *App) Connect(label, addr, symKey string, generateKeyPair bool) error {
	if _, ok := app.channels[label]; ok {
		return ChannelExists
	}

	p, err := pages.NewChannelPage(app, label, addr, symKey, generateKeyPair)
	if err != nil {
		return err
	}

	app.channels[label] = p

	app.pages.AddAndSwitchToPage(label, p, true)
	return nil
}

func (app *App) Disconnect(label string) error {
	page, ok := app.channels[label]
	if !ok {
		return ChannelNotExists
	}

	app.changePage(INFOPAGE)
	app.pages.RemovePage(label)

	delete(app.channels, label)

	page.Kill()
	return nil
}

func (app *App) Draw() {
	app.app.Draw()
}

func (app *App) Exit(err error) {
	for _, page := range app.channels {
		page.Kill()
	}

	app.app.Stop()

	if err != nil {
		log.Printf("Exited with: %v", err)
	}
}

func (app App) GetLogLevel() loglevel.LogLevel {
	return loglevel.Debug
}

func (app *App) GetChannelList() ([]string, error) {
	if len(app.channels) == 0 {
		return nil, NoChannels
	}

	var channels []string
	for label := range app.channels {
		channels = append(channels, label)
	}

	return channels, nil
}

func (app *App) ShowConnectForm() error {
	app.connectForm.Clear()
	app.changePage(CONNECTFORM)
	return nil
}

func (app *App) ShowInfoPage() error {
	app.changePage(INFOPAGE)
	return nil
}

func (app *App) ShowKeyForm(channel *channel.Channel) error {
	app.keyForm.Clear()
	app.keyForm.SetChannel(channel)
	app.changePage(KEYFORM)
	return nil
}
