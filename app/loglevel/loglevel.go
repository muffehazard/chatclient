package loglevel

type LogLevel int

const (
	Error LogLevel = iota
	Info
	Debug
)
