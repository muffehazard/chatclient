package pages

import (
	"fmt"
	"strings"
	"time"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	"gitlab.com/muffehazard/chatclient/app/loglevel"
	"gitlab.com/muffehazard/chatclient/app/pages/channel"
	"gitlab.com/muffehazard/chatclient/app/pages/commandhandler"
)

type ChannelPage struct {
	app            IApp
	label          string
	channel        *channel.Channel
	grid           *tview.Grid
	output         *tview.TextView
	input          *tview.InputField
	commandHandler *commandhandler.CommandHandler
}

func NewChannelPage(app IApp, label, addr, symKey string, generateKeyPair bool) (*ChannelPage, error) {
	cp := &ChannelPage{
		app:    app,
		label:  label,
		grid:   tview.NewGrid(),
		output: tview.NewTextView(),
		input:  tview.NewInputField(),
	}

	cp.output.SetDynamicColors(true)
	cp.output.SetTitle(label)
	cp.output.SetBorder(true)
	cp.input.SetDoneFunc(cp.handleInput)

	cp.grid.SetRows(0, 1)
	cp.grid.AddItem(cp.output, 0, 0, 1, 1, 0, 0, false)
	cp.grid.AddItem(cp.input, 1, 0, 1, 1, 0, 0, true)

	var err error
	cp.channel, err = channel.New(cp, addr, symKey, generateKeyPair)
	if err != nil {
		return nil, err
	}

	cp.commandHandler = commandhandler.New(cp)

	return cp, nil
}

// Private methods
func (cp *ChannelPage) setInput(text string) {
	cp.input.SetText(text)
}

func (cp *ChannelPage) clearInput() {
	cp.input.SetText("")
}

func (cp *ChannelPage) handleInput(key tcell.Key) {
	input := cp.input.GetText()
	if len(input) == 0 {
		return
	}

	switch key {
	case tcell.KeyEnter:
		cp.handleEnter(input)
	case tcell.KeyTAB:
		cp.handleTab(input)
	}
}

func (cp *ChannelPage) handleEnter(input string) {
	input = strings.TrimSpace(input)

	if strings.HasPrefix(input, "/") {
		err, candidates := cp.commandHandler.Call(input)
		if err == commandhandler.CommandNotFound {
			cp.WriteError("Command not found: %s", input)
			return
		} else if err == commandhandler.CommandNotUnique {
			cp.WriteInfo("Command candidates:")
			for _, cand := range candidates {
				cp.WriteInfo("  %s", cand)
			}
		} else if err != nil {
			cp.WriteError("Error running command: %v", err)
		} else {
			cp.clearInput()
		}
	} else if strings.HasPrefix(input, "@") {
		split := strings.Split(input, " ")
		recipient := strings.TrimPrefix(split[0], "@")
		message := strings.Join(split[1:], " ")
		if cp.channel.SendPrivateMsg(message, recipient) {
			cp.clearInput()
		}
	} else if strings.HasPrefix(input, "!") {
		if cp.channel.SendPublicMsg(strings.TrimPrefix(input, "!")) {
			cp.clearInput()
		}
	} else {
		if cp.channel.SendMsg(input) {
			cp.clearInput()
		}
	}
}

func (cp *ChannelPage) handleTab(input string) {
	input = strings.TrimSpace(input)
	if strings.HasPrefix(input, "/") {
		common, unique := cp.commandHandler.Complete(input)
		if unique {
			cp.setInput(fmt.Sprintf("%s ", common))
			return
		}

		cp.setInput(fmt.Sprintf("%s", common))
	} else if strings.HasPrefix(input, "@") {
		split := strings.Split(input, " ")
		user := strings.TrimPrefix(split[0], "@")
		msg := strings.Join(split[1:], " ")

		common, candidates := cp.channel.CompleteUser(user)
		if candidates != nil {
			cp.WriteInfo("User candidates:")
			for _, candidate := range candidates {
				cp.WriteInfo("  %s", candidate)
			}
		}

		cp.setInput(fmt.Sprintf("@%s %s", common, msg))
	}
}

// Public methods
func (cp *ChannelPage) Kill() {
	cp.channel.Kill()
}

func (cp ChannelPage) GetLabel() string {
	return cp.label
}

func (cp *ChannelPage) SetUsername(username string) {
	cp.input.SetLabel(username)
}

func (cp *ChannelPage) ChangeChannel(channel string) error {
	err := cp.app.ChangeChannel(channel)
	if err != nil {
		return fmt.Errorf("Error changing channels: %v", err)
	}

	cp.clearInput()
	return nil
}

func (cp *ChannelPage) ChannelList() error {
	channels, err := cp.app.GetChannelList()
	if err != nil {
		return fmt.Errorf("Error displaying channel list: %v", err)
	}

	cp.WriteInfo("Available channels:")
	for _, channel := range channels {
		cp.WriteInfo("  %s", channel)
	}

	return nil
}

func (cp *ChannelPage) ConnectForm() error {
	err := cp.app.ShowConnectForm()
	if err != nil {
		return fmt.Errorf("Error showing connect form: %v", err)
	}

	return nil
}

func (cp *ChannelPage) Disconnect() error {
	err := cp.app.Disconnect(cp.label)
	if err != nil {
		return fmt.Errorf("Error disconnecting: %v", err)
	}

	return nil
}

func (cp *ChannelPage) KeyForm() error {
	err := cp.app.ShowKeyForm(cp.channel)
	if err != nil {
		return fmt.Errorf("Error displaying key form: %v", err)
	}

	return nil
}

func (cp *ChannelPage) InfoPage() error {
	err := cp.app.ShowInfoPage()
	if err != nil {
		return fmt.Errorf("Error displaying info page")
	}

	return nil
}

func (cp *ChannelPage) Exit() error {
	cp.app.Exit(nil)
	return nil
}

func (cp *ChannelPage) WriteMsg(format string, v ...interface{}) {
	timestamp := fmt.Sprintf("[%s] ", time.Now().Format("15:04:05"))
	_, err := fmt.Fprintf(cp, timestamp+format+"\n", v...)
	if err != nil {
		cp.app.WriteErrMsg("%s: Error writing output: %v", cp, err)
	}

	cp.app.Draw()
}

func (cp *ChannelPage) WriteError(format string, v ...interface{}) {
	if cp.app.GetLogLevel() >= loglevel.Error {
		cp.WriteMsg("<ERROR> "+format, v...)
	}
}

func (cp *ChannelPage) WriteInfo(format string, v ...interface{}) {
	if cp.app.GetLogLevel() >= loglevel.Info {
		cp.WriteMsg("<INFO> "+format, v...)
	}
}

func (cp *ChannelPage) WriteDebug(format string, v ...interface{}) {
	if cp.app.GetLogLevel() >= loglevel.Debug {
		cp.WriteMsg("<DEBUG> "+format, v...)
	}
}

func (cp ChannelPage) String() string {
	return fmt.Sprintf("Channel %v", cp.label)
}

func (cp *ChannelPage) Write(p []byte) (n int, err error) {
	return cp.output.Write(p)
}

func (cp ChannelPage) Channel() *channel.Channel {
	return cp.channel
}

func (cp ChannelPage) HasFocus() bool {
	return cp.grid.HasFocus()
}

func (cp ChannelPage) Draw(screen tcell.Screen) {
	cp.grid.Draw(screen)
}

func (cp ChannelPage) GetRect() (int, int, int, int) {
	return cp.grid.GetRect()
}

func (cp *ChannelPage) SetRect(x, y, width, height int) {
	cp.grid.SetRect(x, y, width, height)
}

func (cp *ChannelPage) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return cp.grid.InputHandler()
}

func (cp *ChannelPage) Focus(delegate func(p tview.Primitive)) {
	cp.grid.Focus(delegate)
}

func (cp *ChannelPage) Blur() {
	cp.grid.Blur()
}

func (cp *ChannelPage) GetFocusable() tview.Focusable {
	return cp.grid.GetFocusable()
}
