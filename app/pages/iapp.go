package pages

import (
	"gitlab.com/muffehazard/chatclient/app/loglevel"
	"gitlab.com/muffehazard/chatclient/app/pages/channel"
)

type IApp interface {
	Draw()
	GetLogLevel() loglevel.LogLevel
	ShowKeyForm(channel *channel.Channel) error
	GetChannelList() ([]string, error)
	ShowConnectForm() error
	ShowInfoPage() error
	Connect(label, address, symKey string, generateKeyPair bool) error
	Disconnect(label string) error
	ChangeChannel(channelName string) error
	WriteErrMsg(format string, v ...interface{})

	Exit(error)
}
