package pages

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

// Connection form
type ConnectForm struct {
	app          IApp
	form         *tview.Form
	label        string
	address      string
	symKey       string
	generateKeys bool
}

func NewConnectForm(app IApp) *ConnectForm {
	cf := &ConnectForm{
		app:  app,
		form: tview.NewForm(),
	}

	cf.reset()

	return cf
}

// Private methods
func (cf *ConnectForm) reset() {
	cf.form.Clear(true)

	cf.label = ""
	cf.address = "ws://localhost"
	cf.symKey = ""
	cf.generateKeys = false

	cf.form.AddInputField("Channel label:", cf.label, 0, nil, func(text string) { cf.label = text })
	cf.form.AddInputField("Address:", cf.address, 0, nil, func(text string) { cf.address = text })
	cf.form.AddPasswordField("Symmetric key:", cf.symKey, 0, '*', func(text string) { cf.symKey = text })
	cf.form.AddCheckbox("Generate public key pair:", cf.generateKeys, func(checked bool) { cf.generateKeys = checked })
	cf.form.AddButton("Connect", cf.handleConnect)
	cf.form.AddButton("Clear", cf.handleClear)
	cf.form.AddButton("Back", cf.handleBack)
}

func (cf *ConnectForm) handleConnect() {
	defer cf.reset()

	err := cf.app.Connect(cf.label, cf.address, cf.symKey, cf.generateKeys)
	if err != nil {
		cf.app.WriteErrMsg("Error connecting to %v: %v", cf.address, err)
	}
}

func (cf *ConnectForm) handleClear() {
	cf.reset()
}

func (cf *ConnectForm) handleBack() {
	cf.app.ShowInfoPage()
}

// Public methods
func (cf *ConnectForm) Clear() {
	cf.reset()
}

func (cf ConnectForm) HasFocus() bool {
	return cf.form.HasFocus()
}

func (cf ConnectForm) Draw(screen tcell.Screen) {
	cf.form.Draw(screen)
}

func (cf ConnectForm) GetRect() (int, int, int, int) {
	return cf.form.GetRect()
}

func (cf *ConnectForm) SetRect(x, y, width, height int) {
	cf.form.SetRect(x, y, width, height)
}

func (cf *ConnectForm) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return cf.form.InputHandler()
}

func (cf *ConnectForm) Focus(delegate func(p tview.Primitive)) {
	cf.form.Focus(delegate)
}

func (cf *ConnectForm) Blur() {
	cf.form.Blur()
}

func (cf *ConnectForm) GetFocusable() tview.Focusable {
	return cf.form.GetFocusable()
}
