package pages

import (
	"fmt"
	"strings"
	"time"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	"gitlab.com/muffehazard/chatclient/app/loglevel"
	"gitlab.com/muffehazard/chatclient/app/pages/commandhandler"
)

// Page for displaying info
type InfoPage struct {
	app            IApp
	grid           *tview.Grid
	output         *tview.TextView
	input          *tview.InputField
	commandHandler *commandhandler.CommandHandler
}

func NewInfoPage(app IApp) *InfoPage {
	ip := &InfoPage{
		app:    app,
		grid:   tview.NewGrid(),
		output: tview.NewTextView(),
		input:  tview.NewInputField(),
	}

	ip.commandHandler = commandhandler.New(ip)

	ip.output.SetBorder(true)
	ip.output.SetTitle("INFO")
	ip.output.SetDynamicColors(true)
	ip.input.SetDoneFunc(ip.handleInput)

	ip.grid.SetRows(0, 1)
	ip.grid.AddItem(ip.output, 0, 0, 1, 1, 0, 0, false)
	ip.grid.AddItem(ip.input, 1, 0, 1, 1, 0, 0, true)

	return ip
}

// Private methods
func (ip *InfoPage) setInput(text string) {
	ip.input.SetText(text)
}

func (ip *InfoPage) clearInput() {
	ip.input.SetText("")
}

func (ip *InfoPage) handleInput(key tcell.Key) {
	input := ip.input.GetText()
	if len(input) == 0 {
		return
	}

	switch key {
	case tcell.KeyEnter:
		ip.handleEnter(input)
	case tcell.KeyTAB:
		ip.handleTab(input)
	}
}

func (ip *InfoPage) handleEnter(input string) {
	input = strings.TrimSpace(input)

	if strings.HasPrefix(input, "/") {
		err, candidates := ip.commandHandler.Call(input)
		if err == commandhandler.CommandNotFound {
			ip.WriteError("Command not found: %s", input)
		} else if err == commandhandler.CommandNotUnique {
			ip.WriteInfo("Command candidates:")
			for _, cand := range candidates {
				ip.WriteInfo("  %s", cand)
			}
		} else if err != nil {
			ip.WriteError("Error running command: %v", err)
		}

		ip.clearInput()
	}
}

func (ip *InfoPage) handleTab(input string) {
	input = strings.TrimSpace(input)

	if strings.HasPrefix(input, "/") {
		common, unique := ip.commandHandler.Complete(input)
		if unique {
			ip.setInput(fmt.Sprintf("%s ", common))
			return
		}

		ip.setInput(common)
	}
}

// Public methods
func (ip *InfoPage) WriteMsg(format string, v ...interface{}) error {
	timestamp := fmt.Sprintf("[%s] ", time.Now().Format("15:04:05"))
	_, err := fmt.Fprintf(ip, timestamp+format+"\n", v...)
	if err != nil {
		return err
	}

	ip.app.Draw()
	return nil
}

func (ip *InfoPage) WriteError(format string, v ...interface{}) {
	if ip.app.GetLogLevel() >= loglevel.Error {
		ip.WriteMsg("<ERROR> "+format, v...)
	}
}

func (ip *InfoPage) WriteInfo(format string, v ...interface{}) {
	if ip.app.GetLogLevel() >= loglevel.Info {
		ip.WriteMsg("<INFO> "+format, v...)
	}
}

func (ip *InfoPage) WriteDebug(format string, v ...interface{}) {
	if ip.app.GetLogLevel() >= loglevel.Debug {
		ip.WriteMsg("<DEBUG> "+format, v...)
	}
}

func (ip InfoPage) String() string {
	return "InfoPage"
}

func (ip *InfoPage) Write(p []byte) (n int, err error) {
	return ip.output.Write(p)
}

func (ip *InfoPage) ChangeChannel(channel string) error {
	err := ip.app.ChangeChannel(channel)
	if err != nil {
		return fmt.Errorf("Error changing channels: %v", err)
	}

	return nil
}

func (ip *InfoPage) ChannelList() error {
	channels, err := ip.app.GetChannelList()
	if err != nil {
		return fmt.Errorf("Error showing channel list: %v", err)
	}

	ip.WriteInfo("Available channels:")
	for _, channel := range channels {
		ip.WriteInfo("  %s", channel)
	}

	return nil
}

func (ip *InfoPage) ConnectForm() error {
	err := ip.app.ShowConnectForm()
	if err != nil {
		return fmt.Errorf("Error showing connect form: %v", err)
	}

	return nil
}

func (ip *InfoPage) Disconnect() error {
	return nil
}

func (ip *InfoPage) InfoPage() error {
	return nil
}

func (ip *InfoPage) Exit() error {
	ip.app.Exit(nil)
	return nil
}

func (ip *InfoPage) KeyForm() error {
	return nil
}

func (ip InfoPage) HasFocus() bool {
	return ip.grid.HasFocus()
}

func (ip InfoPage) Draw(screen tcell.Screen) {
	ip.grid.Draw(screen)
}

func (ip InfoPage) GetRect() (int, int, int, int) {
	return ip.grid.GetRect()
}

func (ip *InfoPage) SetRect(x, y, width, height int) {
	ip.grid.SetRect(x, y, width, height)
}

func (ip *InfoPage) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return ip.grid.InputHandler()
}

func (ip *InfoPage) Focus(delegate func(p tview.Primitive)) {
	ip.grid.Focus(delegate)
}

func (ip *InfoPage) Blur() {
	ip.grid.Blur()
}

func (ip *InfoPage) GetFocusable() tview.Focusable {
	return ip.grid.GetFocusable()
}
