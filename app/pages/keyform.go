package pages

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	"gitlab.com/muffehazard/chatclient/app/pages/channel"
)

// Key change form
type KeyForm struct {
	app               IApp
	channel           *channel.Channel
	form              *tview.Form
	key               string
	generatePublicKey bool
}

func NewKeyForm(app IApp) *KeyForm {
	kf := &KeyForm{
		app:  app,
		form: tview.NewForm(),
	}

	kf.Clear()

	return kf
}

// Private methods
func (kf *KeyForm) handleSetKey() {
	defer kf.form.Clear(true)

	if kf.channel == nil {
		kf.app.WriteErrMsg("Missing channel in key form")
		return
	}

	kf.channel.SetSymmetricKey(kf.key)

	if kf.generatePublicKey {
		err := kf.channel.GeneratePublicKeyPair()
		if err != nil {
			kf.app.WriteErrMsg("Error generating new public key pair: %v", err)
		}
	}

	err := kf.app.ChangeChannel(kf.channel.Label())
	if err != nil {
		kf.app.WriteErrMsg("Error changing to channel: %v", err)
	}
}

func (kf *KeyForm) handleClear() {
	kf.Clear()
}

func (kf *KeyForm) handleBack() {
	kf.app.ShowInfoPage()
}

// Public methods
func (kf *KeyForm) SetChannel(c *channel.Channel) {
	if c != nil {
		kf.form.SetTitle(c.String())
	} else {
		kf.form.SetTitle("<Channel not selected>")
	}

	kf.channel = c
}

func (kf *KeyForm) Clear() {
	kf.form.Clear(true)

	kf.key = ""
	kf.generatePublicKey = false

	kf.form.AddPasswordField("Symmetric key:", "", 0, '*', func(text string) { kf.key = text })
	kf.form.AddCheckbox("Generate public key pair", false, func(checked bool) { kf.generatePublicKey = checked })
	kf.form.AddButton("Set", kf.handleSetKey)
	kf.form.AddButton("Clear", kf.handleClear)
	kf.form.AddButton("Back", kf.handleBack)

}

func (kf KeyForm) HasFocus() bool {
	return kf.form.HasFocus()
}

func (kf KeyForm) Draw(screen tcell.Screen) {
	kf.form.Draw(screen)
}

func (kf KeyForm) GetRect() (int, int, int, int) {
	return kf.form.GetRect()
}

func (kf *KeyForm) SetRect(x, y, width, height int) {
	kf.form.SetRect(x, y, width, height)
}

func (kf *KeyForm) InputHandler() func(event *tcell.EventKey, setFocus func(p tview.Primitive)) {
	return kf.form.InputHandler()
}

func (kf *KeyForm) Focus(delegate func(p tview.Primitive)) {
	kf.form.Focus(delegate)
}

func (kf *KeyForm) Blur() {
	kf.form.Blur()
}

func (kf *KeyForm) GetFocusable() tview.Focusable {
	return kf.form.GetFocusable()
}
