package commandhandler

import (
	"strings"

	"gitlab.com/muffehazard/chatclient/app/commandradix"
)

type cmdError string

func (e cmdError) Error() string { return string(e) }

const CommandNotUnique = cmdError("Command not unique")
const CommandNotFound = cmdError("Command not found")
const MultipleChannelsWithConnect = cmdError("Multiple channels with connect")

// Handles general commands
type CommandHandler struct {
	page         IPage
	commandRadix *commandradix.CommandRadix
}

func New(page IPage) *CommandHandler {
	ch := &CommandHandler{
		page:         page,
		commandRadix: commandradix.NewCommandRadix(),
	}

	ch.commandRadix.Insert("/info", ch.handleInfo)
	ch.commandRadix.Insert("/channel", ch.handleChannel)
	ch.commandRadix.Insert("/join", ch.handleConnect)
	ch.commandRadix.Insert("/connect", ch.handleConnect)
	ch.commandRadix.Insert("/disconnect", ch.handleDisconnect)
	ch.commandRadix.Insert("/list", ch.handleList)
	ch.commandRadix.Insert("/key", ch.handleKey)
	ch.commandRadix.Insert("/exit", ch.handleExit)

	return ch
}

func (ch *CommandHandler) Complete(command string) (string, bool) {
	split := strings.Split(command, " ")
	cmd := strings.ToLower(split[0])

	common, _, f := ch.commandRadix.Get(cmd)
	return strings.Join(append([]string{common}, split[1:]...), " "), f != nil
}

func (ch *CommandHandler) Call(command string) (error, []string) {
	split := strings.Split(command, " ")
	cmd := strings.ToLower(split[0])
	params := split[1:]

	_, candidates, f := ch.commandRadix.Get(cmd)
	if f != nil {
		err := f(params)
		if err != nil {
			return err, nil
		}

		return nil, nil
	}

	if len(candidates) == 0 {
		return CommandNotFound, nil
	}

	return CommandNotUnique, candidates
}

func (ch CommandHandler) handleInfo([]string) error {
	return ch.page.InfoPage()
}

func (ch CommandHandler) handleChannel(params []string) error {
	if len(params) == 0 {
		return ch.page.ChannelList()
	}

	return ch.page.ChangeChannel(params[0])
}

func (ch CommandHandler) handleConnect([]string) error {
	return ch.page.ConnectForm()
}

func (ch CommandHandler) handleDisconnect([]string) error {
	return ch.page.Disconnect()
}

func (ch CommandHandler) handleList([]string) error {
	return ch.page.ChannelList()
}

func (ch CommandHandler) handleKey([]string) error {
	return ch.page.KeyForm()
}

func (ch CommandHandler) handleExit([]string) error {
	return ch.page.Exit()
}
