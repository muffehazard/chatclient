package commandhandler

type IPage interface {
	ChangeChannel(label string) error
	ChannelList() error
	ConnectForm() error
	Disconnect() error
	KeyForm() error
	InfoPage() error

	Exit() error
}
