package channel

type IChannelPage interface {
	GetLabel() string
	SetUsername(string)
	WriteMsg(format string, v ...interface{})
	WriteError(format string, v ...interface{})
	WriteInfo(format string, v ...interface{})
	WriteDebug(format string, v ...interface{})
}
