package channel

import (
	"fmt"
	"net/http"
	"time"

	"github.com/armon/go-radix"
	"github.com/gorilla/websocket"
	"gitlab.com/muffehazard/chatclient/message"
	"gitlab.com/muffehazard/chatclient/symkey"
	"gitlab.com/muffehazard/chatclient/user"
	"gitlab.com/muffehazard/chatclient/util"
)

// Handles connection to server
type Channel struct {
	channelPage IChannelPage
	conn        *websocket.Conn
	symKey      symkey.SymKey
	user        *user.PrivUser
	userRadix   *radix.Tree
	killChan    chan struct{}
}

func New(channelPage IChannelPage, addr, symKey string, generateKeyPair bool) (*Channel, error) {
	channel := &Channel{
		channelPage: channelPage,
		userRadix:   radix.New(),
		killChan:    make(chan struct{}),
	}

	channel.SetSymmetricKey(symKey)
	if generateKeyPair {
		err := channel.GeneratePublicKeyPair()
		if err != nil {
			return nil, err
		}
	}

	dialer := websocket.Dialer{
		Proxy:             http.ProxyFromEnvironment,
		HandshakeTimeout:  45 * time.Second,
		EnableCompression: true,
	}

	var err error
	channel.conn, _, err = dialer.Dial(addr, nil)
	if err != nil {
		return nil, err
	}

	channel.conn.SetPingHandler(func(string) error {
		channel.conn.WriteMessage(websocket.PongMessage, nil)
		return nil
	})

	go channel.run()

	channel.channelPage.SetUsername(channel.Username())

	return channel, nil
}

// Private methods
func (c *Channel) sendMsg(msg *message.Message) bool {
	w, err := c.conn.NextWriter(websocket.BinaryMessage)
	if err != nil {
		c.channelPage.WriteError("Error creating writer: %v", err)
		return false
	}

	err = msg.Encode(w, c.user)
	if err != nil {
		c.channelPage.WriteError("Error encoding message: %v", err)
		return false
	}

	err = w.Close()
	if err != nil {
		c.channelPage.WriteError("Error closing writer: %v", err)
		return false
	}

	return true
}

func (c *Channel) waitForKill() {
	<-c.killChan

	err := c.conn.Close()
	if err != nil && websocket.IsUnexpectedCloseError(err, websocket.CloseNormalClosure) {
		c.channelPage.WriteError("%Unexpected websocket close on kill: %v", err)
	}
}

func (c *Channel) readMsg() {
	for {
		_, msg, err := c.conn.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseNormalClosure, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				c.channelPage.WriteError("Unexpected websocket read error: %v", err)
			}

			c.channelPage.WriteInfo("Disconnected")
			return
		}

		decoded, err := message.Decode(msg, c.user, c.symKey)
		if err != nil {
			c.channelPage.WriteDebug("Failed to decode message: %v", err)
			continue
		}

		if decoded.SignatureValid {
			if _, found := c.userRadix.Get(decoded.Sender.String()); !found {
				c.userRadix.Insert(decoded.Sender.String(), *decoded.Sender)
			}
		}

		c.channelPage.WriteMsg("%s", decoded)
	}
}

func (c *Channel) run() {
	defer c.conn.Close()

	go c.waitForKill()

	c.readMsg()
}

// Public methods
func (c *Channel) Kill() {
	close(c.killChan)
}
func (c Channel) Label() string {
	return c.channelPage.GetLabel()
}

func (c Channel) String() string {
	return fmt.Sprintf("%s : %s", c.channelPage.GetLabel(), c.conn.RemoteAddr().String())
}

func (c Channel) Username() string {
	if c.user != nil {
		return fmt.Sprintf("[%s]", c.user)
	}

	return "<Anonymous>"
}

func (c *Channel) SetSymmetricKey(key string) {
	if len(key) == 0 {
		c.symKey = nil
		c.channelPage.WriteInfo("Unset symmetric key")
		return
	}

	c.symKey = symkey.New(key)
	c.channelPage.WriteInfo("Set symmetric key")
}

func (c *Channel) GeneratePublicKeyPair() error {
	c.channelPage.WriteInfo("Generating key pair...")
	var err error
	c.user, err = user.NewPriv()
	if err != nil {
		return err
	}

	c.channelPage.WriteInfo("Done generating key pair")
	c.channelPage.SetUsername(c.Username())
	return nil
}

func (c *Channel) SendMsg(content string) bool {
	if c.symKey != nil {
		return c.SendSymmetricMsg(content)
	}

	return c.SendPublicMsg(content)
}

func (c *Channel) SendPublicMsg(content string) bool {
	return c.sendMsg(message.NewPublic(content))
}

func (c *Channel) SendSymmetricMsg(content string) bool {
	if c.symKey == nil {
		c.channelPage.WriteError("Missing symmetric key")
		return false
	}

	msg, err := message.NewSymmetric(content, c.symKey)
	if err != nil {
		c.channelPage.WriteError("Error creating symmetric message: %v", err)
		return false
	}

	return c.sendMsg(msg)
}

func (c *Channel) SendPrivateMsg(content, recipient string) bool {
	key, found := c.userRadix.Get(recipient)
	if !found {
		c.channelPage.WriteError("No such user: %v", recipient)
		return false
	}

	receiver, ok := key.(user.User)
	if !ok {
		c.channelPage.WriteError("Couldn't fetch user key")
		return false
	}

	msg, err := message.NewAsymmetric(content, receiver)
	if err != nil {
		c.channelPage.WriteError("Error creating private message: %v", err)
		return false
	}

	if !c.sendMsg(msg) {
		return false
	}

	if c.user == nil || c.user.ToUser().Equal(receiver) {
		return true
	}

	msg, err = message.NewAsymmetric(content, c.user.ToUser())
	if err != nil {
		c.channelPage.WriteError("Error creating private echo message: %v", err)
		return false
	}

	return c.sendMsg(msg)
}

func (c Channel) CompleteUser(user string) (string, []string) {
	candidates := []string{}
	c.userRadix.WalkPrefix(user, func(key string, v interface{}) bool {
		candidates = append(candidates, key)
		return false
	})

	common := util.CommonPrefix(candidates)
	if _, found := c.userRadix.Get(common); found {
		return common, nil
	}

	return common, candidates
}
