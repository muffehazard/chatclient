package commandradix

import (
	"github.com/armon/go-radix"
	"gitlab.com/muffehazard/chatclient/util"
)

// Radix tree for commands
type Command struct {
	Key string
	F   func([]string)
}

type CommandRadix struct {
	radix *radix.Tree
}

func NewCommandRadix() *CommandRadix {
	return &CommandRadix{
		radix: radix.New(),
	}
}

func (cr *CommandRadix) Insert(key string, f func([]string) error) {
	cr.radix.Insert(key, f)
}

func (cr CommandRadix) Get(key string) (string, []string, func([]string) error) {
	candidates := []string{}
	cr.radix.WalkPrefix(key, func(key string, v interface{}) bool {
		candidates = append(candidates, key)
		return false
	})

	common := util.CommonPrefix(candidates)
	v, found := cr.radix.Get(common)
	if !found {
		return common, candidates, nil
	}

	return common, candidates, v.(func([]string) error)
}
