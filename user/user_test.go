package user_test

import (
	"bytes"
	"crypto/rsa"
	"testing"

	"gitlab.com/muffehazard/chatclient/user"
	"gotest.tools/assert"
)

const TestData = "TESTDATA"

// Test user comparison
func TestEqual(t *testing.T) {
	t.Parallel()

	// Create user
	u, err := user.NewPriv()
	assert.NilError(t, err)

	// Get user public key
	pub := u.ToUser()

	// Compare with self
	assert.Assert(t, pub.Equal(pub))

	// Create other user
	other, err := user.NewPriv()
	assert.NilError(t, err)

	// Compare with other user
	assert.Assert(t, !pub.Equal(other.ToUser()))

	// Compare empty user
	empty := user.User{}
	assert.Assert(t, empty.Equal(empty))
	assert.Assert(t, !empty.Equal(pub))
	assert.Assert(t, !pub.Equal(empty))
}

// Test JSON marshaller
func TestJSONMarhal(t *testing.T) {
	t.Parallel()

	// Create new user
	u, err := user.NewPriv()
	assert.NilError(t, err)

	// Get user public key
	pub := u.ToUser()

	// Marshal as JSON
	buf, err := pub.MarshalJSON()
	assert.NilError(t, err)

	// Decode user from JSON
	var decoded user.User
	err = decoded.UnmarshalJSON(buf)
	assert.NilError(t, err)
	assert.Assert(t, pub.Equal(decoded))
}

// Test encryption
func TestEncrypt(t *testing.T) {
	t.Parallel()

	// Create user
	u, err := user.NewPriv()
	assert.NilError(t, err)

	// Get user public key
	pub := u.ToUser()

	// Encrypt data with public key
	buf := bytes.NewBufferString(TestData)
	ciphertext, err := pub.Encrypt(buf.Bytes())
	assert.NilError(t, err)

	// Decode with correct private key
	plaintext, err := u.Decrypt(ciphertext)
	assert.NilError(t, err)
	assert.Equal(t, bytes.NewBuffer(plaintext).String(), buf.String())

	// Create other user
	other, err := user.NewPriv()
	assert.NilError(t, err)

	// Decode with wrong private key
	plaintext, err = other.Decrypt(ciphertext)
	assert.ErrorType(t, err, rsa.ErrDecryption)
	assert.Assert(t, plaintext == nil)
}

// Test signature
func TestSign(t *testing.T) {
	t.Parallel()

	// Create user
	u, err := user.NewPriv()
	assert.NilError(t, err)

	// Sign data && verify with correct public key
	signable := bytes.NewBufferString(TestData)
	signature, err := u.Sign(signable.Bytes())
	assert.NilError(t, err)
	assert.Assert(t, u.ToUser().VerifySignature(signable.Bytes(), signature))

	// Create other user && verify with wrong public key
	other, err := user.NewPriv()
	assert.NilError(t, err)
	assert.Assert(t, !other.ToUser().VerifySignature(signable.Bytes(), signature))
}
