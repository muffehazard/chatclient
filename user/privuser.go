package user

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
)

const bitsize = 4096

// User as rsa private key
type PrivUser rsa.PrivateKey

func NewPriv() (*PrivUser, error) {
	key, err := rsa.GenerateKey(rand.Reader, bitsize)
	if err != nil {
		return nil, err
	}

	usr := PrivUser(*key)
	return &usr, nil
}

func (pu PrivUser) ToUser() User {
	return User(pu.PublicKey)
}

func (pu PrivUser) String() string {
	return pu.ToUser().String()
}

func (pu PrivUser) Decrypt(ciphertext []byte) ([]byte, error) {
	key := rsa.PrivateKey(pu)
	return rsa.DecryptPKCS1v15(rand.Reader, &key, ciphertext)
}

func (pu PrivUser) Sign(data []byte) ([]byte, error) {
	key := rsa.PrivateKey(pu)
	hash := sha256.Sum256(data)
	return rsa.SignPKCS1v15(rand.Reader, &key, crypto.SHA256, hash[:])
}
