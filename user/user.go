package user

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
)

var ErrDecyption = rsa.ErrDecryption

// User as rsa public key
type User rsa.PublicKey

func (u User) Equal(other User) bool {
	if u.N == nil && other.N == nil {
		return u.E == other.E
	}

	if u.N == nil || other.N == nil {
		return false
	}

	return u.N.Cmp(other.N) == 0 && u.E == other.E
}

func (u User) MarshalJSON() ([]byte, error) {
	return json.Marshal(u.marshal())
}

func (u *User) UnmarshalJSON(buf []byte) error {
	var data []byte
	err := json.Unmarshal(buf, &data)
	if err != nil {
		return err
	}

	key, err := x509.ParsePKCS1PublicKey(data)
	if err != nil {
		return err
	}

	u.N = key.N
	u.E = key.E

	return nil
}

func (u User) String() string {
	sum := sha256.Sum256(u.marshal())
	return base64.URLEncoding.EncodeToString(sum[:])
}

func (u User) Encrypt(cleartext []byte) ([]byte, error) {
	key := rsa.PublicKey(u)
	return rsa.EncryptPKCS1v15(rand.Reader, &key, cleartext)
}

func (u User) VerifySignature(data []byte, signature []byte) bool {
	key := rsa.PublicKey(u)
	hash := sha256.Sum256(data)
	return rsa.VerifyPKCS1v15(&key, crypto.SHA256, hash[:], signature) == nil
}

func (u User) marshal() []byte {
	key := rsa.PublicKey(u)
	return x509.MarshalPKCS1PublicKey(&key)
}
