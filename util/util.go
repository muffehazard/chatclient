package util

func CommonPrefix(l []string) string {
	if len(l) == 0 {
		return ""
	} else if len(l) == 1 {
		return l[0]
	}

	min, max := l[0], l[0]
	for _, s := range l[1:] {
		switch {
		case s < min:
			min = s
		case s > max:
			max = s
		}
	}

	for i := 0; i < len(min) && i < len(max); i++ {
		if min[i] != max[i] {
			return min[:i]
		}
	}

	return min
}
