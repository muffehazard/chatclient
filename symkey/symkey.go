package symkey

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"io"
)

// Symmetric key
// Uses SHA256 sum of user inputted password as key
// GCM as cipher algorithm
type symkeyErr string

func (e symkeyErr) Error() string { return string(e) }

const NonceLengthMismatch = symkeyErr("Nonce length mismatch")
const DecryptFailed = symkeyErr("Decrypt failed")

func getGcmBlock(key []byte) (cipher.AEAD, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("Cipher: %v", err)
	}

	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, fmt.Errorf("GCM: %v", err)
	}

	return gcm, nil
}

type Ciphertext struct {
	Ciphertext []byte `json:"ciphertext"`
	Nonce      []byte `json:"nonce"`
}

type SymKey []byte

func New(pass string) SymKey {
	sum := sha256.Sum256([]byte(pass))
	return sum[:]
}

func (s SymKey) Encrypt(msg []byte) (*Ciphertext, error) {
	gcm, err := getGcmBlock(s)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, fmt.Errorf("Nonce: %v", err)
	}

	ciphertext := gcm.Seal(nil, nonce, msg, nil)

	return &Ciphertext{
		Ciphertext: ciphertext,
		Nonce:      nonce,
	}, nil
}

func (s SymKey) Decrypt(c *Ciphertext) ([]byte, error) {
	gcm, err := getGcmBlock(s)
	if err != nil {
		return nil, err
	}

	if len(c.Nonce) != gcm.NonceSize() {
		return nil, NonceLengthMismatch
	}

	plaintext, err := gcm.Open(nil, c.Nonce, c.Ciphertext, nil)
	if err != nil {
		return nil, DecryptFailed
	}

	return plaintext, nil
}
