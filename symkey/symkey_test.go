package symkey_test

import (
	"bytes"
	"testing"

	"gitlab.com/muffehazard/chatclient/symkey"
	"gotest.tools/assert"
)

const TestKey = "TESTKEY"
const TestOtherKey = "OTHERKEY"
const TestData = "TESTTEST"

// Test symmetric encryption
func TestEncrypt(t *testing.T) {
	t.Parallel()

	// Create symmetric key
	key := symkey.New(TestKey)

	// Encrypt data
	buf := bytes.NewBufferString(TestData)
	ciphertext, err := key.Encrypt(buf.Bytes())
	assert.NilError(t, err)

	// Decrypt with correct key
	plaintext, err := key.Decrypt(ciphertext)
	assert.NilError(t, err)
	assert.Equal(t, bytes.NewBuffer(plaintext).String(), buf.String())

	// Decrypt with wrong key
	other := symkey.New(TestOtherKey)
	plaintext, err = other.Decrypt(ciphertext)
	assert.Error(t, err, symkey.DecryptFailed.Error())
	assert.Assert(t, plaintext == nil)
}
