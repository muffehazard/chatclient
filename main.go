package main

import (
	"log"

	"gitlab.com/muffehazard/chatclient/app"
)

func main() {
	app := app.New()

	if err := app.Run(); err != nil {
		log.Fatal(err)
	}
}
