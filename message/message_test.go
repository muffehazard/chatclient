package message_test

import (
	"bytes"
	"testing"

	"gitlab.com/muffehazard/chatclient/message"
	"gitlab.com/muffehazard/chatclient/symkey"
	"gitlab.com/muffehazard/chatclient/user"
	"gotest.tools/assert"
)

const TestKey = "TESTKEY"
const TestOtherKey = "OTHERKEY"
const TestContent = "TESTTEST"

// Create an invalid empty message
// test that ErrNoMessage is received on decode
func TestInvalidEmpty(t *testing.T) {
	t.Parallel()

	// Empty message
	var msg message.Message

	// Encode
	var buf bytes.Buffer
	err := msg.Encode(&buf, nil)
	assert.NilError(t, err)

	// Decode
	decoded, err := message.Decode(buf.Bytes(), nil, nil)
	assert.Error(t, err, message.ErrNoMessage.Error())
	assert.Assert(t, decoded == nil)
}

// Create public message with no signature
// Test decode
func TestPublicUnsigned(t *testing.T) {
	t.Parallel()

	// Create public
	msg := message.NewPublic(TestContent)

	// Encode
	var buf bytes.Buffer
	err := msg.Encode(&buf, nil)
	assert.NilError(t, err)

	// Decode
	decoded, err := message.Decode(buf.Bytes(), nil, nil)
	assert.NilError(t, err)
	assert.Assert(t, decoded != nil)

	// Verify
	assert.Equal(t, decoded.MsgType, message.PubMsg)
	assert.Assert(t, decoded.Sender == nil)
	assert.Equal(t, decoded.Signed, false)
	assert.Equal(t, decoded.SignatureValid, false)
	assert.Assert(t, decoded.Message != nil)
	assert.Equal(t, decoded.Message.String(), TestContent)
}

// Test public signed message
// Same as above + signature verification
func TestPublicSigned(t *testing.T) {
	t.Parallel()

	// Create public
	msg := message.NewPublic(TestContent)

	// Create signer
	u, err := user.NewPriv()
	assert.NilError(t, err)
	assert.Assert(t, u != nil)

	// Get signer public key
	pub := u.ToUser()

	// Encode + sign
	var buf bytes.Buffer
	err = msg.Encode(&buf, u)
	assert.NilError(t, err)

	// Decode
	decoded, err := message.Decode(buf.Bytes(), nil, nil)
	assert.NilError(t, err)
	assert.Assert(t, decoded != nil)

	// Verify
	assert.Equal(t, decoded.MsgType, message.PubMsg)
	assert.Assert(t, decoded.Sender != nil)
	assert.DeepEqual(t, *decoded.Sender, pub)
	assert.Equal(t, decoded.Signed, true)
	assert.Equal(t, decoded.SignatureValid, true)
	assert.Assert(t, decoded.Message != nil)
	assert.Equal(t, decoded.Message.String(), TestContent)
}

// Test symmetric unsigned message
// Test decode with valid key, invalid key and no key
func TestSymmetricUnsigned(t *testing.T) {
	t.Parallel()

	// Create symmetric key
	key := symkey.New(TestKey)

	// Create symmetric message
	msg, err := message.NewSymmetric(TestContent, key)
	assert.NilError(t, err)
	assert.Assert(t, msg != nil)

	// Encode
	var buf bytes.Buffer
	err = msg.Encode(&buf, nil)
	assert.NilError(t, err)

	// Decode with correct key
	decoded, err := message.Decode(buf.Bytes(), nil, key)
	assert.NilError(t, err)
	assert.Assert(t, decoded != nil)

	// Verify
	assert.Equal(t, decoded.MsgType, message.SymMsg)
	assert.Assert(t, decoded.Sender == nil)
	assert.Equal(t, decoded.Signed, false)
	assert.Equal(t, decoded.SignatureValid, false)
	assert.Assert(t, decoded.Message != nil)
	assert.Equal(t, decoded.Message.String(), TestContent)

	// Decode with no key
	decoded, err = message.Decode(buf.Bytes(), nil, nil)
	assert.Error(t, err, message.ErrNoValidKey.Error())
	assert.Assert(t, decoded == nil)

	// Decode with wrong key
	other := symkey.New(TestOtherKey)
	decoded, err = message.Decode(buf.Bytes(), nil, other)
	assert.Error(t, err, message.ErrNoValidKey.Error())
	assert.Assert(t, decoded == nil)
}

// Test symmetric signed message
// Same as above + signature verification
func TestSymmetricSigned(t *testing.T) {
	t.Parallel()

	// Create signer
	u, err := user.NewPriv()
	assert.NilError(t, err)
	assert.Assert(t, u != nil)

	// Get signer public key
	pub := u.ToUser()

	// Create symmetric key
	key := symkey.New(TestKey)

	// Create symmetric message
	msg, err := message.NewSymmetric(TestContent, key)
	assert.NilError(t, err)
	assert.Assert(t, msg != nil)

	// Encode
	var buf bytes.Buffer
	err = msg.Encode(&buf, u)
	assert.NilError(t, err)

	// Decode with correct key
	decoded, err := message.Decode(buf.Bytes(), nil, key)
	assert.NilError(t, err)
	assert.Assert(t, decoded != nil)

	// Verify
	assert.Equal(t, decoded.MsgType, message.SymMsg)
	assert.Assert(t, decoded.Sender != nil)
	assert.DeepEqual(t, *decoded.Sender, pub)
	assert.Equal(t, decoded.Signed, true)
	assert.Equal(t, decoded.SignatureValid, true)
	assert.Assert(t, decoded.Message != nil)
	assert.Equal(t, decoded.Message.String(), TestContent)

	// Decode with no key
	decoded, err = message.Decode(buf.Bytes(), nil, nil)
	assert.Error(t, err, message.ErrNoValidKey.Error())
	assert.Assert(t, decoded == nil)

	// Decode with wrong key
	other := symkey.New(TestOtherKey)
	decoded, err = message.Decode(buf.Bytes(), nil, other)
	assert.Error(t, err, message.ErrNoValidKey.Error())
	assert.Assert(t, decoded == nil)
}

// Test asymmetric unsigned message
// Test decode with correct key, invalid key and no key
func TestAsymmetricUnsigned(t *testing.T) {
	t.Parallel()

	// Create target user
	u, err := user.NewPriv()
	assert.NilError(t, err)
	assert.Assert(t, u != nil)

	// Get target public key
	pub := u.ToUser()

	// Create asymmetric message
	msg, err := message.NewAsymmetric(TestContent, pub)
	assert.NilError(t, err)
	assert.Assert(t, msg != nil)

	// Encode
	var buf bytes.Buffer
	err = msg.Encode(&buf, nil)
	assert.NilError(t, err)

	// Decode
	decoded, err := message.Decode(buf.Bytes(), u, nil)
	assert.NilError(t, err)
	assert.Assert(t, decoded != nil)

	// Verify
	assert.Equal(t, decoded.MsgType, message.AsymMsg)
	assert.Assert(t, decoded.Sender == nil)
	assert.Equal(t, decoded.Signed, false)
	assert.Equal(t, decoded.SignatureValid, false)
	assert.Assert(t, decoded.Message != nil)
	assert.Equal(t, decoded.Message.String(), TestContent)

	// Decode with no key
	decoded, err = message.Decode(buf.Bytes(), nil, nil)
	assert.Error(t, err, message.ErrNoValidKey.Error())
	assert.Assert(t, decoded == nil)

	// Create other user
	other, err := user.NewPriv()
	assert.NilError(t, err)
	assert.Assert(t, other != nil)

	// Decode with other user
	decoded, err = message.Decode(buf.Bytes(), other, nil)
	assert.Error(t, err, message.ErrNoValidKey.Error())
	assert.Assert(t, decoded == nil)
}

// Test asymmetric signed message
// Same as above + signature verification
func TestAsymmetricSigned(t *testing.T) {
	t.Parallel()

	// Create target user
	u, err := user.NewPriv()
	assert.NilError(t, err)

	// Get user public key
	pub := u.ToUser()

	// Create asymmetric message
	msg, err := message.NewAsymmetric(TestContent, pub)
	assert.NilError(t, err)

	// Encode
	var buf bytes.Buffer
	err = msg.Encode(&buf, u)
	assert.NilError(t, err)

	// Decode
	decoded, err := message.Decode(buf.Bytes(), u, nil)
	assert.NilError(t, err)

	// Verify
	assert.Equal(t, decoded.MsgType, message.AsymMsg)
	assert.Assert(t, decoded.Sender != nil)
	assert.DeepEqual(t, *decoded.Sender, pub)
	assert.Equal(t, decoded.Signed, true)
	assert.Equal(t, decoded.SignatureValid, true)
	assert.Assert(t, decoded.Message != nil)
	assert.Equal(t, decoded.Message.String(), TestContent)

	// Decode with no key
	decoded, err = message.Decode(buf.Bytes(), nil, nil)
	assert.Error(t, err, message.ErrNoValidKey.Error())
	assert.Assert(t, decoded == nil)

	// Create other user
	other, err := user.NewPriv()
	assert.NilError(t, err)
	assert.Assert(t, other != nil)

	// Decode with other user
	decoded, err = message.Decode(buf.Bytes(), other, nil)
	assert.Error(t, err, message.ErrNoValidKey.Error())
	assert.Assert(t, decoded == nil)
}
