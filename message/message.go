package message

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/muffehazard/chatclient/symkey"
	"gitlab.com/muffehazard/chatclient/user"
)

const (
	PubMsg  = "Pub"
	SymMsg  = "Sym"
	AsymMsg = "Asym"
)

type msgError string

func (m msgError) Error() string {
	return string(m)
}

const ErrNoValidKey = msgError("No valid key")
const ErrNoMessage = msgError("No message content")

// Decoded message
type DecodedMsg struct {
	MsgType        string
	Sender         *user.User
	Signed         bool
	SignatureValid bool
	Message        *bytes.Buffer
}

func (dm DecodedMsg) String() string {
	msgString := fmt.Sprintf("(%s) %s", dm.MsgType, dm.Message)

	if dm.Sender == nil {
		return fmt.Sprintf("<anonymous> %s", msgString)
	}

	if !dm.Signed {
		return fmt.Sprintf("[[#FF4500]UNSIGNED[#FFFFFF] %s] %s", dm.Sender, msgString)
	}

	if !dm.SignatureValid {
		return fmt.Sprintf("[[#FF0000]INVALID[#FFFFFF] %s] %s", dm.Sender, msgString)
	}

	return fmt.Sprintf("[[#00FF00]Signed[#FFFFFF] %s] %s", dm.Sender, msgString)
}

// Sendable message
type Message struct {
	Sender     *user.User  `json:"sender"`
	Public     *public     `json:"public"`
	Symmetric  *symmetric  `json:"symmetric"`
	Asymmetric *asymmetric `json:"asymmetric"`
	Signature  []byte      `json:"signature"`
}

func NewPublic(content string) *Message {
	return &Message{
		Public: &public{
			Msg: []byte(content),
		},
	}
}

func NewSymmetric(content string, symKey symkey.SymKey) (*Message, error) {
	symmetric, err := newSymmetric(content, symKey)
	if err != nil {
		return nil, err
	}
	return &Message{
		Symmetric: symmetric,
	}, nil
}

func NewAsymmetric(content string, receiver user.User) (*Message, error) {
	asym, err := newAsymmetric(content, receiver)
	if err != nil {
		return nil, err
	}

	return &Message{
		Asymmetric: asym,
	}, nil
}

func Decode(buf []byte, u *user.PrivUser, symKey symkey.SymKey) (*DecodedMsg, error) {
	msg := &Message{}
	err := json.Unmarshal(buf, msg)
	if err != nil {
		return nil, err
	}

	decodedMsg := &DecodedMsg{
		Sender:         msg.Sender,
		Signed:         msg.Signature != nil,
		SignatureValid: msg.verifySignature(),
	}

	if msg.Asymmetric != nil {
		if u == nil || !msg.Asymmetric.checkUser(*u) {
			return nil, ErrNoValidKey
		}

		decoded, err := msg.Asymmetric.decode(u)
		if err != nil {
			if isNotValidAsymmetricKeyError(err) {
				return nil, ErrNoValidKey
			}

			return nil, err
		}

		decodedMsg.MsgType = AsymMsg
		decodedMsg.Message = bytes.NewBuffer(decoded)
		return decodedMsg, nil
	}

	if msg.Symmetric != nil {
		if symKey == nil {
			return nil, ErrNoValidKey
		}

		dec, err := msg.Symmetric.decode(symKey)
		if err != nil {
			if isNotValidSymmetricKeyError(err) {
				return nil, ErrNoValidKey
			}

			return nil, err
		}

		decodedMsg.Message = bytes.NewBuffer(dec)
		decodedMsg.MsgType = SymMsg
		return decodedMsg, nil
	}

	if msg.Public != nil {
		decodedMsg.Message = bytes.NewBuffer(msg.Public.Msg)
		decodedMsg.MsgType = PubMsg
		return decodedMsg, nil
	}

	return nil, ErrNoMessage
}

func (msg Message) Encode(w io.Writer, signer *user.PrivUser) error {
	err := msg.sign(signer)
	if err != nil {
		return err
	}

	return json.NewEncoder(w).Encode(msg)
}

// Generates signable JSON
func (msg Message) signable() ([]byte, error) {
	msg.Signature = nil
	return json.Marshal(msg)
}

// Signs message with signer as signee
func (msg *Message) sign(signer *user.PrivUser) error {
	if signer == nil {
		return nil
	}

	sigUser := signer.ToUser()
	msg.Sender = &sigUser

	signable, err := msg.signable()
	if err != nil {
		return err
	}

	msg.Signature, err = signer.Sign(signable)
	if err != nil {
		return err
	}

	return nil
}

// Verifies message signature
func (msg Message) verifySignature() bool {
	if msg.Signature == nil || msg.Sender == nil {
		return false
	}

	signable, err := msg.signable()
	if err != nil {
		return false
	}

	return msg.Sender.VerifySignature(signable, msg.Signature)
}
