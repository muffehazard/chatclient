package message

import (
	"crypto/hmac"
	"crypto/sha256"

	"gitlab.com/muffehazard/chatclient/symkey"
)

type symErr string

func (e symErr) Error() string { return string(e) }

const HMACMismatch = symErr("HMAC Mismatch")

// Symmetric message
// Calculates HMAC for validation
type symmetric struct {
	HMAC []byte             `json:"HMAC"`
	Msg  *symkey.Ciphertext `json:"msg"`
}

func newSymmetric(msg string, key symkey.SymKey) (*symmetric, error) {
	sym := &symmetric{}
	var err error
	sym.Msg, err = key.Encrypt([]byte(msg))
	if err != nil {
		return nil, err
	}

	sym.HMAC, err = sym.calcHMAC(key)
	if err != nil {
		return nil, err
	}

	return sym, nil
}

func (s symmetric) decode(key symkey.SymKey) ([]byte, error) {
	valid, err := s.validateHMAC(key)
	if err != nil {
		return nil, err
	}

	if !valid {
		return nil, HMACMismatch
	}

	return key.Decrypt(s.Msg)
}

func (s symmetric) calcHMAC(key symkey.SymKey) ([]byte, error) {
	h := hmac.New(sha256.New, key)
	_, err := h.Write(s.Msg.Ciphertext)
	if err != nil {
		return nil, err
	}

	_, err = h.Write(s.Msg.Nonce)
	if err != nil {
		return nil, err
	}

	return h.Sum(nil), nil
}

func (s symmetric) validateHMAC(key symkey.SymKey) (bool, error) {
	h, err := s.calcHMAC(key)
	if err != nil {
		return false, err
	}

	return hmac.Equal(h, s.HMAC), nil
}

func isNotValidSymmetricKeyError(err error) bool {
	return err == HMACMismatch || err == symkey.NonceLengthMismatch || err == symkey.DecryptFailed
}
