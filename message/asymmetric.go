package message

import (
	"gitlab.com/muffehazard/chatclient/user"
)

// Asymmetric message
type asymmetric struct {
	Receiver user.User `json:"receiver"`
	Msg      []byte    `json:"msg"`
}

func newAsymmetric(msg string, u user.User) (*asymmetric, error) {
	asym := &asymmetric{
		Receiver: u,
	}

	var err error
	asym.Msg, err = u.Encrypt([]byte(msg))
	if err != nil {
		return nil, err
	}

	return asym, nil
}

func (a asymmetric) checkUser(u user.PrivUser) bool {
	return u.ToUser().Equal(a.Receiver)
}

func (a asymmetric) decode(user *user.PrivUser) ([]byte, error) {
	return user.Decrypt(a.Msg)
}

func isNotValidAsymmetricKeyError(err error) bool {
	return err == user.ErrDecyption
}
