module gitlab.com/muffehazard/chatclient

go 1.12

require (
	github.com/armon/go-radix v1.0.0
	github.com/gdamore/tcell v1.1.2
	github.com/google/go-cmp v0.3.0 // indirect
	github.com/gorilla/websocket v1.4.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/rivo/tview v0.0.0-20190515161233-bd836ef13b4b
	golang.org/x/text v0.3.2 // indirect
	gotest.tools v2.2.0+incompatible
)
